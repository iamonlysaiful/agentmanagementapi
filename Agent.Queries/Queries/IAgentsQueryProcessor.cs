﻿using Agent.API.Model.Agents;
using Agent.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agent.Queries.Queries
{
    public interface IAgentsQueryProcessor
    {
        IQueryable<Agents> Get();
        Agents Get(int id);
        Task<Agents> Create(CreateAgentsModel model);
        Task<Agents> Update(int id, UpdateAgentsModel model);
        Task Delete(int id);
    }
}
