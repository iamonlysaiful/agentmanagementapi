﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agent.API.Common.Exceptions;
using Agent.API.Model.Agents;
using Agent.Data.Access.DAL;
using Agent.Data.Model;

namespace Agent.Queries.Queries
{
    public class AgentsQueryProcessor : IAgentsQueryProcessor
    {
        private readonly IUnitOfWork _uow;
        public AgentsQueryProcessor(IUnitOfWork uow)
        {
            _uow = uow;

        }
        public async Task<Agents> Create(CreateAgentsModel model)
        {
            try
            {
                var item = new Agents
                {

                    Code = model.Code,
                    Email = model.Email,
                    Name = model.Name,
                    Street = model.Street,
                    City = model.City,
                    State = model.State,
                    Zip = model.Zip,
                    Country = model.Country,
                    Mobile = model.Mobile,
                    Phone = model.Phone,
                    ContactPerson = model.ContactPerson,
                    ReferredBy = model.ReferredBy,
                    Logo = model.Logo,
                    Status = model.Status,
                    Balance = model.Balance,
                    LoginUrl = model.LoginUrl,
                    SecurityCode = model.SecurityCode,
                    SMTPServer = model.SMTPServer,
                    SMTPPort = model.SMTPPort,
                    SMTPUsername = model.SMTPUsername,
                    SMTPPassword = model.SMTPPassword,
                    Deleted = model.Deleted,
                    CreatedOnUtc =DateTime.Now,
                    UpdatedOnUtc = DateTime.Now,
                    CurrentBalance = model.CurrentBalance
                };

                _uow.Add(item);
                await _uow.CommitAsync();

                return item;
            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }

        public async Task Delete(int id)
        {
            var agent = GetQuery().FirstOrDefault(u => u.BusinessId == id);

            if (agent == null)
            {
                throw new NotFoundException("Agent is not found");
            }

            if (Convert.ToBoolean(agent.Deleted)) return;

            agent.Deleted = true;
            await _uow.CommitAsync();
        }

        public IQueryable<Agents> Get()
        {
            var query = GetQuery();
            return query;
        }

        public Agents Get(int id)
        {

            try
            {
                var agent = GetQuery().FirstOrDefault(x => x.BusinessId == id);

                if (agent == null)
                {
                    throw new NotFoundException("Agent is not found");
                }

                return agent;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        public async Task<Agents> Update(int id, UpdateAgentsModel model)
        {
            var agent = GetQuery().FirstOrDefault(x => x.BusinessId == id);

            if (agent == null)
            {
                throw new NotFoundException("Agent is not found");
            }

            agent.Name = model.Name;
            agent.Code = model.Code;
            agent.Mobile = model.Mobile;
            agent.Email = model.Email;
            agent.Status = model.Status;
            agent.Balance = model.Balance;
            agent.Deleted =false;
            agent.UpdatedOnUtc = DateTime.Now;

            await _uow.CommitAsync();
            return agent;
        }

        //==============================================
        private IQueryable<Agents> GetQuery()
        {
            var q = _uow.Query<Agents>()
                .Where(x => !(x.Deleted));

            //if (!_securityContext.IsAdministrator)
            //{
            //    var userId = _securityContext.User.Id;
            //    q = q.Where(x => x.UserId == userId);
            //}

            return q;
        }

    }
}
