﻿using System.Linq;
using System.Threading.Tasks;
using Agent.API.Filters;
using Agent.API.Maps;
using Agent.API.Model.Agents;
using Agent.Data.Model;
using Agent.Queries.Queries;
using Microsoft.AspNetCore.Mvc;

namespace Agent.API.Controllers
{
    [Route("api/[controller]")]
    public class AgentsController : Controller
    {
        private readonly IAgentsQueryProcessor _query;
        private readonly IAutoMapper _mapper;

        public AgentsController(IAgentsQueryProcessor query, IAutoMapper mapper)
        {
            _query = query;
            _mapper = mapper;
        }

        [HttpGet]
        [QueryableResult]
        public IQueryable<AgentsModel> Get()
        {
            var result = _query.Get();
            var models = _mapper.Map<Agents, AgentsModel>(result);
            return models;
        }

        [HttpGet("{id}")]
        public AgentsModel Get(int id)
        {
            var item = _query.Get(id);
            var model = _mapper.Map<AgentsModel>(item);
            return model;
        }

        [HttpPost]
        [ValidateModel]
        public async Task<AgentsModel> Post([FromBody]CreateAgentsModel requestModel)
        {
            var item = await _query.Create(requestModel);
            var model = _mapper.Map<AgentsModel>(item);
            return model;
        }

        [HttpPut("{id}")]
        [ValidateModel]
        public async Task<AgentsModel> Put(int id, [FromBody]UpdateAgentsModel requestModel)
        {
            var item = await _query.Update(id, requestModel);
            var model = _mapper.Map<AgentsModel>(item);
            return model;
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _query.Delete(id);
        }
    }
}