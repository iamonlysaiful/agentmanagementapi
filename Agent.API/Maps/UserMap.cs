﻿using System.Linq;
using Agent.API.Model.Agents;
using Agent.Data.Model;
using AutoMapper;

namespace Agent.API.Maps
{
    public class UserMap : IAutoMapperTypeConfigurator
    {
        public void Configure(IMapperConfigurationExpression configuration)
        {
            var map = configuration.CreateMap<Agents, AgentsModel>();
            //map.ForMember(x => x.Roles, x => x.MapFrom(u => u.Roles.Select(r => r.Role.Name).ToArray()));
        }
    }
}