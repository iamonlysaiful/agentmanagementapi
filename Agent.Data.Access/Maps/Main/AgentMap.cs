﻿using Agent.Data.Access.Maps.Common;
using Agent.Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agent.Data.Access.Maps.Main
{
    public class AgentMap : IMap
    {
        public void Visit(ModelBuilder builder)
        {
            builder.Entity<Agents>()
                .ToTable("BusinessEntities")
                .HasKey(x => x.BusinessId);
        }
    }
}
