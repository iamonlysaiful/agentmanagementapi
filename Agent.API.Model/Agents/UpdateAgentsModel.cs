﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agent.API.Model.Agents
{
    public class UpdateAgentsModel
    {
        public int Status { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public decimal Balance { get; set; }
        public string Code { get; set; }
        //public byte Deleted { get; set; }
        public bool Deleted { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
       
    }
}
